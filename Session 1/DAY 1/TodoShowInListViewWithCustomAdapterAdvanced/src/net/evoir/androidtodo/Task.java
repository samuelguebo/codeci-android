package net.evoir.androidtodo;

import java.util.Date;
import java.util.Random;

import net.evoir.androidtodo.withcustomadapteradvanced.R;

public class Task {

	private String title; 
	private String desc; 
	private Date pubDate;
	private int imgId;
	public int getImgId() {
		return imgId;
	}
	public void setImgId(int imgId) {
		this.imgId = imgId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getPubDate() {
		return pubDate;
	}
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}
		
	public String toString() {
			return this.title + "\n" +this.desc +" "+ this.pubDate;
	
	}
	
	public Task(String title, String desc, Date pubDate) {
		super();
		this.title = title;
		this.desc= desc;
		this.pubDate= pubDate;
		this.imgId= generateImgId();
	}
	
	int[] stars= {
			R.drawable.star1,R.drawable.star2,R.drawable.star3,R.drawable.star4,R.drawable.star5,R.drawable.star6,R.drawable.star7,R.drawable.star8};

	private int generateImgId(){
		Random r = new Random();
		int min =0;
		int max =7;
		int random = r.nextInt(max- min +1) + min;
		return stars[random];
	}
}
