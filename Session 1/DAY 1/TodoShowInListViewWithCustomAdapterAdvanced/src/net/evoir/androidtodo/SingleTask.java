package net.evoir.androidtodo;

import net.evoir.androidtodo.withcustomadapteradvanced.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class SingleTask extends Activity{
	String TAG = "colibri";
	public SingleTask() {
		// TODO Auto-generated constructor stub
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//TODO 1. set the main layout of the Activity
		setContentView(R.layout.tasks_list_single);		
		MyViewHolder mViewHolder;
		
	//grab the data stored in the intent
		Bundle i = getIntent().getExtras();
		
	// set the data to the views using the Viewholder method
		mViewHolder = new MyViewHolder();
		mViewHolder.mTitle = detail(R.id.singletaskTitle,  i.getString("mTitle"));
		mViewHolder.mDesc = detail(R.id.singletaskDesc, i.getString("mDesc"));
		
		mViewHolder.mDate = detail(R.id.singletaskDate, i.getString("mDate"));
		mViewHolder.mImg = detail(R.id.singletaskImg, i.getInt("mImg"));
		
		//Log.e(TAG,"onCreate of SingleTask() mDate:"+mDate);


		
	}
	private TextView detail(int resId, String text) { 
		TextView tv = (TextView) findViewById(resId); tv.setText(text); 
		return tv; 
	}
	private ImageView detail(int resId, int imgId) { 
		//Drawable mImage = new BitmapDrawable(getResources(), image);
		ImageView iv = (ImageView) findViewById(resId); 
		iv.setImageResource(imgId);

		return iv;		
	}
	public class MyViewHolder { 
		TextView mTitle, mDesc,mDate;  
		ImageView mImg;
	}

}
