package net.evoir.androidtodo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;

import net.evoir.androidtodo.withcustomadapteradvanced.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TaskAdapter extends BaseAdapter{

	private Context mContext;
	ArrayList<Task> mlist = new ArrayList<Task>();
	LayoutInflater inflater;
	//create a dynamic drawable ID in order to match the drawable present in res (star1,star2 etc)
	
			
	public TaskAdapter(Context context,ArrayList<Task> list) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.mlist= list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Task getItem(int position) {
		// TODO Auto-generated method stub
		return mlist.get(position);
		
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyViewHolder mViewHolder;
		//1. Instantiate the layoutInflater
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(convertView == null) {
		//2. inflate the view using the inflater we just created
			
			convertView= inflater.inflate(R.layout.tasks_list_item, null);
			mViewHolder = new MyViewHolder();
			convertView.setTag(mViewHolder);
		}
		else {
			mViewHolder = (MyViewHolder) convertView.getTag();
		}

		mViewHolder.mTitle = detail(convertView,R.id.taskTitle,mlist.get(position).getTitle());
		mViewHolder.mDesc = detail(convertView,R.id.taskDesc,mlist.get(position).getDesc());
		//format the date
		String fmDate= new SimpleDateFormat("dd/MM/yyyy").format(mlist.get(position).getPubDate());
		
		mViewHolder.mDate = detail(convertView,R.id.taskDate,fmDate);
		mViewHolder.mImg =  (ImageView) convertView.findViewById(R.id.taskImg);
				//detail(convertView,R.id.taskImg,mlist.get(position).getImgId());
		mViewHolder.mImg.setImageResource(mlist.get(position).getImgId());
		
		return convertView;
	}
	private TextView detail(View v, int resId, String text) { 
		TextView tv = (TextView) v.findViewById(resId); tv.setText(text); 
		return tv; 
	}
	/*private ImageView detail(View v, int resId, int id) { 
		ImageView iv = (ImageView) v.findViewById(resId); 
		//generate a random number for in order to show a random star (star1,star12,star4)
		Random r = new Random();
		int min =0;
		int max =7;
		int random = r.nextInt(max- min +1) + min;
		iv.setImageResource(stars[random]); 
		return iv; 
		}*/
	
	private class MyViewHolder { 
		TextView mTitle, mDesc,mDate;  
		ImageView mImg;
	}

	
}
