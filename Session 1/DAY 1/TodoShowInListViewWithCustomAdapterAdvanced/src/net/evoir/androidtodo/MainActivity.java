package net.evoir.androidtodo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.evoir.androidtodo.withcustomadapteradvanced.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	String TAG= "colibri";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//TODO 1. declare the variables I will need: the context for the layout inflating, the listview
		final Context mContext = this.getApplicationContext();
		ListView lv;
		TextView item;

		super.onCreate(savedInstanceState);
		//TODO 1. set the main layout of the Activity
		setContentView(R.layout.tasks_list_main);
		//TODO 2. create a new list 
		final ArrayList<Task> tasks = new ArrayList<Task>();
		//TODO 3. populate the list
		for (int i = 1; i <= 50; i++) {
			//since I will need to set tomorrow's date I'll instantiate a calendar
			// sorry about that, I could not remember how u did during the session :)
			Calendar calendar = Calendar.getInstance();
		
			calendar.add(Calendar.DAY_OF_YEAR,i); // add +1 to the current day
			
			Date tomorrow = (Date) calendar.getTime();

			//build the new Task
			Task task = new Task("Task #"+i,"I am task number "+i,tomorrow); 
			tasks.add(task);
		}
	
		//TODO 4. get a reference to the ListView (xml file)
		lv = (ListView) findViewById(R.id.tasksList);
		//TODO 5. set the adapter ,Three parameters : context, resource (xml single item), data (the array we previously created 
		final TaskAdapter taskAdapter = new TaskAdapter(mContext, tasks);
		lv.setAdapter(taskAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				// when user clicks on ListView Item , onItemClick is called 
                // with position and View of the item which is clicked		
				//TaskAdapter taskAdapter = new TaskAdapter(mContext, tasks);
				String mTitle = taskAdapter.getItem(position).getTitle();
				String mDesc = taskAdapter.getItem(position).getDesc();
				//TextView tDate = taskAdapter.getItem(position).getPubDate().toString();
				String mDate = taskAdapter.getItem(position).getPubDate().toString();
				Log.e(TAG,"onItemClick mDate:"+mDate);
				//ImageView mImg= (ImageView) findViewById(R.id.taskImg);

					
				
				//start a new Intent and then start a new Activity where we'll send all this data
				Intent i = new Intent(mContext,SingleTask.class);
				
				i.putExtra("mTitle",taskAdapter.getItem(position).getTitle());
				i.putExtra("mDesc", taskAdapter.getItem(position).getDesc());
				i.putExtra("mDate", new SimpleDateFormat("dd/MM/yyyy").format(taskAdapter.getItem(position).getPubDate()));
				
				
				// Make *drawable* out of the picture
				//Bitmap bitmap = ((BitmapDrawable)mImg.getDrawable()).getBitmap();
				i.putExtra("mImg", taskAdapter.getItem(position).getImgId());
 
				startActivity(i);
				
				finish();
			}
		});

				
	}
	/*public int getImageResource(ImageView iv) {
	    return (Integer) iv.getTag();
	}*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
