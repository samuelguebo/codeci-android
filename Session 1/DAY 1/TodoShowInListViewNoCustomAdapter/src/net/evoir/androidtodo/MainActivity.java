package net.evoir.androidtodo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//TODO 1. declare the variables I will need: the context for the layout inflating, the listview
		Context mContext = this.getApplicationContext();
		ListView lv;
		TextView item;


		super.onCreate(savedInstanceState);
		//TODO 1. set the main layout of the Activity
		setContentView(R.layout.tasks_list_main);
		//TODO 2. create a new list 
		List tasks = new ArrayList<Task>();
		//TODO 3. populate the list
		for (int i = 0; i < 10; i++) {
			//since I will need to set tomorrow's date I'll instantiate a calendar
			// sorry about that, I could not remember how u did during the session :)
			Calendar calendar = Calendar.getInstance();
		
			calendar.add(Calendar.DAY_OF_YEAR,i); // add +1 to the current day
			
			Date tomorrow = (Date) calendar.getTime();

			//build the new Task
			Task task = new Task("Task #"+i,"I am number "+i,tomorrow); 
			tasks.add(task);
		}
		
		//TODO 4. get a reference to the ListView (xml file)
		lv = (ListView) findViewById(R.id.tasksList);
		//TODO 5. set the adapter ,Three parameters : context, resource (xml single item), data (the array we previously created 
		lv.setAdapter(new ArrayAdapter<Task>(mContext, android.R.layout.simple_list_item_1,tasks));

				
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
