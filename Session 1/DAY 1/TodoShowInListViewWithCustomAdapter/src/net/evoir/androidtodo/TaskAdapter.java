package net.evoir.androidtodo;

import java.util.ArrayList;

import net.evoir.androidtodo.customadapter.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TaskAdapter extends BaseAdapter{

	private Context mContext;
	ArrayList<Task> mlist = new ArrayList<Task>();
	LayoutInflater inflater;
	public TaskAdapter(Context context,ArrayList<Task> list) {
		super();
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.mlist= list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Task getItem(int position) {
		// TODO Auto-generated method stub
		return mlist.get(position);
		
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyViewHolder mViewHolder;
		//1. Instantiate the layoutInflater
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(convertView == null) {
		//2. inflate the view using the inflater we just created
			
			convertView= inflater.inflate(R.layout.tasks_list_item, null);
			mViewHolder = new MyViewHolder();
			convertView.setTag(mViewHolder);
		}
		else {
			mViewHolder = (MyViewHolder) convertView.getTag();
		}

		mViewHolder.mTitle = detail(convertView,R.id.tasksListItem,mlist.get(position).toString());
		
		return convertView;
	}
	private TextView detail(View v, int resId, String text) { 
		TextView tv = (TextView) v.findViewById(resId); tv.setText(text); 
		return tv; 
		}
	
	private class MyViewHolder { 
		TextView mTitle, mDesc,mDate;  
	}

	
}
