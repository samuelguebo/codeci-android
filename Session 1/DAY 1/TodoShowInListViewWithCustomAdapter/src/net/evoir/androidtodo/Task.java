package net.evoir.androidtodo;

import java.util.Date;

public class Task {

	private String title; 
	private String desc; 
	private Date pubDate;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getPubDate() {
		return pubDate;
	}
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}
		
	public String toString() {
			return this.title + "\n" +this.desc +" "+ this.pubDate;
	
	}
	
	public Task(String title, String desc, Date pubDate) {
		super();
		this.title = title;
		this.desc= desc;
		this.pubDate= pubDate;
	}

}
