package net.evoir.androidtodo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//TODO 1. set the main layout of the Activity
		setContentView(R.layout.tasks_list_main);
		//TODO 2. create a new list 
		List tasks = new ArrayList<Task>();
		//TODO 3. populate the list
		for (int i = 0; i < 10; i++) {
			//since I will need to set tomorrow's date I'll instanciate a calendar
			// sorry about that, I could not remember how u did during the session :)
			Calendar calendar = Calendar.getInstance();
		
			calendar.add(Calendar.DAY_OF_YEAR,i); // add +1 to the current day
			
			Date tomorrow = (Date) calendar.getTime();

			//build the new Task
			Task task = new Task("Task #"+i,"I am number "+i,tomorrow); 
			tasks.add(task);
		}
		
		String text ="";
		// loop through each row of the array and then build the final text line by line
		for (int i=0;i<tasks.size();i++) {
			text+=tasks.get(i).toString() + "\n" ;
		}
		
		// I select my TextView that will hold the tasks
				TextView tv_tasks = (TextView) findViewById(R.id.tasksList);
				tv_tasks.setText(text);
				
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
