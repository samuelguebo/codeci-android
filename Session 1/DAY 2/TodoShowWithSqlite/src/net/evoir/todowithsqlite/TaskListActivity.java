package net.evoir.todowithsqlite;

import java.sql.SQLException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import net.evoir.androidtodo.R;

import com.j256.ormlite.dao.Dao;

public class TaskListActivity extends Activity {

    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_task_list);

        this.mContext = this;
        
        Dao<Task, Integer> dao;
        try {
            dao = Model.getHelper(this.mContext).getDao(Task.class);
            final List<Task> taskList = dao.queryForAll();
            final ListView taskListView = (ListView) this.findViewById(R.id.tasksList);
            TaskAdapter taskAdapter = new TaskAdapter(this.mContext, taskList);
            taskListView.setAdapter(taskAdapter);
            taskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Task task = taskList.get(position);
                    Intent intent = new Intent(TaskListActivity.this.mContext, TaskDetailActivity.class);
                    intent.putExtra("id", task.getId());
                	Log.i("mytag", "Longclicked : "+position);

                    TaskListActivity.this.mContext.startActivity(intent);
                }

            });
/*            taskListView.setLongClickable(true);
            taskListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                	
                	Toast bread = Toast.makeText(TaskListActivity.this.mContext, "Hey", Toast.LENGTH_LONG);
                	bread.show();
                	Log.i("mytag", "Longclicked : "+pos);
                    Task task = taskList.get(pos);

                	Intent intent = new Intent(TaskListActivity.this.mContext, TaskEditActivity.class);
                    intent.putExtra("id", task.getId());
                	
                	return true;                
					}
            });*/



        } catch (SQLException e) {
            Log.e("TaskListActivity", e.getMessage());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.getMenuInflater().inflate(R.menu.task_list, menu);
        return true;
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add) {
            startActivity(new Intent(mContext, TaskAddActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    
}
