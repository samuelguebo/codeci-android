package net.evoir.todowithsqlite;

import java.sql.SQLException;
import java.util.Date;

import net.evoir.androidtodo.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TaskAddActivity extends Activity {

   // private String mTitle;
   // private String mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_add);
        
        final EditText titleEditText = (EditText) findViewById(R.id.taskTitle);
        final EditText descriptionEditText = (EditText) findViewById(R.id.taskDescription);
        Button submitButton = (Button) findViewById(R.id.submitButton);
        
        submitButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                if (titleEditText.getText().toString().isEmpty()) {
                    titleEditText.setError("Champ requis");
                } else if (descriptionEditText.getText().toString().isEmpty()) {
                    descriptionEditText.setError("Champ requis");
                } else {
                    Task task = new Task(titleEditText.getText().toString(), descriptionEditText.getText().toString(), new Date());
                    try {
                        Model.getHelper(TaskAddActivity.this).getDao(Task.class).create(task);
                        Toast.makeText(TaskAddActivity.this, "New task created", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TaskAddActivity.this, TaskListActivity.class);
                        TaskAddActivity.this.startActivity(intent);
                    } catch (SQLException e) {
                        Log.e("Task Add Activity", e.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task_add, menu);
        return true;
    }

}
