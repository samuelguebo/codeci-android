package net.evoir.todowithsqlite.android;

import com.j256.ormlite.field.DatabaseField;

public class User extends Model {

    @DatabaseField
    private String password;
    
    @DatabaseField
    private String username;

    public User() {
        
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return this.username;
    }

    
    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
