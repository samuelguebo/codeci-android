package net.evoir.todowithsqlite.android;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;

public class Task extends Model {


    @DatabaseField
    private String title;
    
    @DatabaseField
    private String description;
    
    @DatabaseField
    private Date dueDate;
    
    @DatabaseField
    private Boolean status;
    
    



	public Task() {
        
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }


	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}


       
    public Task(String title, String description, Date dueDate,Boolean status) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.status = status;
    }

    
}
