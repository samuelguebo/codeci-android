package net.evoir.todowithsqlite.android;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;

import net.evoir.iwant.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TaskAdapter extends BaseAdapter {

    private List<Task> mTaskList;
    private Context mContext;
    private LayoutInflater mInflater;
    private Dao<Task, Integer> dao;
    private Task task;
    public TaskAdapter(Context context, List<Task> taskList) {
        this.mContext = context;
        this.mTaskList = taskList;
    }

    @Override
    public int getCount() {
        return mTaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return mTaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
//        return mTaskList.get(position).getId();
    }

    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {
        final Task task = mTaskList.get(position);
        
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewCopy = mInflater.inflate(R.layout.activity_task_item, null);


        TextView taskTitle = (TextView) viewCopy.findViewById(R.id.taskTitle_item);
        taskTitle.setText(task.getTitle());
        
        TextView taskDescription = (TextView) viewCopy.findViewById(R.id.taskDescription_item);
        taskDescription.setText(task.getDescription());
        
        CheckBox taskStatus= (CheckBox) viewCopy.findViewById(R.id.taskStatus_item);
        taskStatus.setChecked(task.getStatus());
        
        TextView taskDueDate= (TextView) viewCopy.findViewById(R.id.taskDueDate_item);
    	taskDueDate.setText(format(task.getDueDate()));
        if (task.getStatus())
        {
        	
        	taskStatus.setText("Done");
        }
        
     // If CheckBox is toggled, update the Task it is tagged with.  
        taskStatus.setOnClickListener( new View.OnClickListener() {  
          public void onClick(View v) {  
            CheckBox cb = (CheckBox) v ;  
 
            //Task task = (Task) cb.getTag();  
            if (cb.isChecked()){
            	setStatus(true, cb);

            }
            else {
            	setStatus(false, cb);
            }

          }

		private void setStatus(boolean b, CheckBox cb) {
			// TODO Auto-generated method stub
			try {

                dao = Model.getHelper(mContext).getDao(Task.class);
            	UpdateBuilder<Task, Integer> updateBuilder = dao.updateBuilder();

            	
            	// set the criteria like you would a QueryBuilder
            	updateBuilder.where().eq("id", task.getId());
            	// update the value of the fields
            	updateBuilder.updateColumnValue("status", b);
            	// update
            	updateBuilder.update();
                Log.v("my tag", "Task Add Activity"+ task.getId()+" "+ task.getStatus());

            	notifyDataSetChanged();
            	
            	if(b) {
	        		task.setStatus(true); 
	            		
	            	cb.setChecked(true);
	        		cb.setText("Done");
	            	Toast bread = Toast.makeText(mContext, "Task "+task.getId()+" is Done", Toast.LENGTH_LONG);
	            	bread.show();
            	}
            	else {
            		task.setStatus(false); 
                	cb.setChecked(false);
            		cb.setText("Not Done");
                	Toast bread = Toast.makeText(mContext, "Task "+task.getId()+" is not Done", Toast.LENGTH_LONG);
                	bread.show();
            	}
            	
               
            } catch (SQLException e) {
                Log.v("my tag", e.getMessage());
                Log.v("my tag", "Task Add Activity"+ task.getId()+" "+ task.getStatus());
            }
		}  
        }); 
        
        return viewCopy;
        
    }
    

    public String format(Date date)
    {
    	
    	DateFormat dfgmt = new java.text.SimpleDateFormat("dd/MM/yyyy");   
        dfgmt.setTimeZone(TimeZone.getTimeZone("GMT")); 
        String gmtTime = dfgmt.format(date);
        return gmtTime;
    }

}

  