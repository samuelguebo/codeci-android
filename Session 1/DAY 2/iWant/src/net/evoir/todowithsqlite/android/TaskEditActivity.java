package net.evoir.todowithsqlite.android;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;

import net.evoir.iwant.R;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TaskEditActivity extends Activity implements
OnClickListener {

   // private String mTitle;
   // private String mDescription;
	private EditText titleEditText, descriptionEditText;
	private Context mContext;
	private TextView dateLabel;
	private Button dateTrigger;
	private DatePicker datePicker;
	private CheckBox statusCheckbox;
	private Button submitButton;
	private static Dao<Task, Integer> dao;
	private int id;
	private static Task task;


 
	private int year;
	private int month;
	private int day;
 
	static final int DATE_DIALOG_ID = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);
        // since id have been sent we just grab it
        if (this.getIntent().getExtras() != null) {
            id = this.getIntent().getExtras().getInt("id");
            Log.i("TaskDetailActivity", "ID DE LA TACHE EST : " + id);

        setContentView(R.layout.activity_task_edit);
        this.mContext = this;
        try {
            dao = Model.getHelper(this).getDao(Task.class);

            task = dao.queryForId(id);
            titleEditText = (EditText) findViewById(R.id.taskTitle_edit);
            titleEditText.setText(task.getTitle());
            descriptionEditText = (EditText) findViewById(R.id.taskDescription_edit);
            descriptionEditText.setText(task.getDescription());
            statusCheckbox = (CheckBox) findViewById(R.id.taskStatus_edit);
            statusCheckbox.setChecked(task.getStatus());
            submitButton = (Button) findViewById(R.id.submitButton_edit);
            
            dateLabel = (TextView) findViewById(R.id.taskDueDateLabel_edit);
            dateLabel.setText(format(task.getDueDate())); // TODO fix this later, date not getting updated
            dateTrigger = (Button) findViewById(R.id.taskDueDateTrigger_edit);
        } catch (SQLException e) {
            Log.e("TaskDetails", e.getMessage());
        }
        
 
     // Variable for storing current date and time
         
        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);
 

  
        dateTrigger.setOnClickListener(this);
        statusCheckbox= (CheckBox) findViewById(R.id.taskStatus_edit);
        statusCheckbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {  
                CheckBox cb = (CheckBox) v ;  
     
                //Task task = (Task) cb.getTag();  
                if (cb.isChecked()){
                	setStatus(true, cb);
                }
                else {
                	setStatus(false, cb);
                }
              }
    		private void setStatus(boolean b, CheckBox cb) {
    			// TODO Auto-generated method stub
    				if(b) {
    	            		
    	            	cb.setChecked(true);
    	        		cb.setText("Done");
                	}
                	else {
                    	cb.setChecked(false);
                		cb.setText("Not Done");
                	}
                	

    		}
        });
        
		
         
       
       
        
        
        submitButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                if (titleEditText.getText().toString().isEmpty()) {
                    titleEditText.setError("Champ requis");
                } else if (descriptionEditText.getText().toString().isEmpty()) {
                    descriptionEditText.setError("Champ requis");
                } else {
                    try {
                    	// we set an update builder
                    	dao = Model.getHelper(TaskEditActivity.this).getDao(Task.class);
                    	UpdateBuilder<Task, Integer> updateBuilder = dao.updateBuilder();
                    	
                    	// set the criteria like you would a QueryBuilder
                    	updateBuilder.where().eq("id", id);
                    	// update the value of the fields
                    	updateBuilder.updateColumnValue("title", titleEditText.getText().toString());
                    	updateBuilder.updateColumnValue("description", descriptionEditText.getText().toString());
                    	updateBuilder.updateColumnValue("status", statusCheckbox.isChecked());
                    	updateBuilder.updateColumnValue("dueDate", toDate(dateLabel.getText().toString()));
                    	// update
                    	updateBuilder.update();


                        Toast.makeText(TaskEditActivity.this, "Task updated", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TaskEditActivity.this, TaskListActivity.class);
                        TaskEditActivity.this.startActivity(intent);
                    } catch (SQLException e) {
                        Log.e("Task EditActivity", e.getMessage());
                    }
                }
            }
        });

        }
        
    }
    
	@Override
	public void onClick(View v) {
		  if (v == dateTrigger) {
			  
	 
	            // Launch Date Picker Dialog
	            DatePickerDialog dpd = new DatePickerDialog(mContext,
	                    new DatePickerDialog.OnDateSetListener() {
	 
	                        @Override
	                        public void onDateSet(DatePicker view, int year,
	                                int monthOfYear, int dayOfMonth) {
	                        	
	                            // Display Selected date in textbox
	                            dateLabel.setText(dayOfMonth+ "/"
	                                    + (monthOfYear + 1) + "/" + year);
	 
	                        }
	                    },year,month,day);
	            
	            dpd.show();
	        }
		  
		
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task_add, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_list) {
            startActivity(new Intent(TaskEditActivity.this, TaskListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
   public Date toDate(String string)
   {
	   	String dateString = string;
	   	//String dateString = "01-12-1900";
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    Date convertedDate = new Date();
	    try {
	        convertedDate = dateFormat.parse(dateString);
	    } catch (ParseException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
		return convertedDate;
	   
   }
   

   public String format(Date date)
   {
   	
   	DateFormat dfgmt = new java.text.SimpleDateFormat("dd/MM/yyyy");   
       dfgmt.setTimeZone(TimeZone.getTimeZone("GMT")); 
       String gmtTime = dfgmt.format(date);
       return gmtTime;
   }
}
