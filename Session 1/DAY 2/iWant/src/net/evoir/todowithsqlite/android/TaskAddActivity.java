package net.evoir.todowithsqlite.android;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import net.evoir.iwant.R;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TaskAddActivity extends Activity implements
OnClickListener {

   // private String mTitle;
   // private String mDescription;
	private Context mContext;
	private TextView dateLabel;
	private Button dateTrigger;
	private DatePicker datePicker;
	private CheckBox statusCheckbox;
 
	private int year;
	private int month;
	private int day;
 
	static final int DATE_DIALOG_ID = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_add);
        this.mContext = this;
        
        final EditText titleEditText = (EditText) findViewById(R.id.taskTitle_edit);
        final EditText descriptionEditText = (EditText) findViewById(R.id.taskDescription_add);
        statusCheckbox = (CheckBox) findViewById(R.id.taskStatus_add);
        Button submitButton = (Button) findViewById(R.id.submitButton_add);
        
        dateLabel = (TextView) findViewById(R.id.taskDueDateLabel_add);
        dateTrigger = (Button) findViewById(R.id.taskDueDateTrigger_add);
 
     // Variable for storing current date and time
         
        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);
 
        // Show current date
         
        dateLabel.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/").append(month + 1).append("/")
                .append(year).append(" "));
  
        dateTrigger.setOnClickListener(this);
        statusCheckbox= (CheckBox) findViewById(R.id.taskStatus_add);
        statusCheckbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {  
                CheckBox cb = (CheckBox) v ;  
     
                //Task task = (Task) cb.getTag();  
                if (cb.isChecked()){
                	setStatus(true, cb);
                }
                else {
                	setStatus(false, cb);
                }
              }
    		private void setStatus(boolean b, CheckBox cb) {
    			// TODO Auto-generated method stub
    				if(b) {
    	            		
    	            	cb.setChecked(true);
    	        		cb.setText("Done");
                	}
                	else {
                    	cb.setChecked(false);
                		cb.setText("Not Done");
                	}
                	

    		}
        });
        
		
         
       
        
        submitButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                if (titleEditText.getText().toString().isEmpty()) {
                    titleEditText.setError("Champ requis");
                } else if (descriptionEditText.getText().toString().isEmpty()) {
                    descriptionEditText.setError("Champ requis");
                } else {
                    Task task = new Task(titleEditText.getText().toString(), descriptionEditText.getText().toString(), toDate(dateLabel.getText().toString()),statusCheckbox.isChecked());
                    Log.v("my tag", "Task Add Activity "+ titleEditText.getText().toString()+" "+descriptionEditText.getText().toString()+" "+toDate(dateLabel.getText().toString())+" "+statusCheckbox.isChecked());

                    try {
                        Model.getHelper(TaskAddActivity.this).getDao(Task.class).create(task);
                        Toast.makeText(TaskAddActivity.this, "New task created", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TaskAddActivity.this, TaskListActivity.class);
                        TaskAddActivity.this.startActivity(intent);
                    } catch (SQLException e) {
                        Log.v("my tag", "Task Add Activity"+ e.getMessage());
                    }
                }
            }
        });
        
    }
    
	@Override
	public void onClick(View v) {
		  if (v == dateTrigger) {
			  
	 
	            // Launch Date Picker Dialog
	            DatePickerDialog dpd = new DatePickerDialog(mContext,
	                    new DatePickerDialog.OnDateSetListener() {
	 
	                        @Override
	                        public void onDateSet(DatePicker view, int year,
	                                int monthOfYear, int dayOfMonth) {
	                        	
	                            // Display Selected date in textbox
	                            dateLabel.setText(dayOfMonth+ "/"
	                                    + (monthOfYear + 1) + "/" + year);
	 
	                        }
	                    },year,month,day);
	            
	            dpd.show();
	        }
		  
		
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task_add, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_list) {
            startActivity(new Intent(TaskAddActivity.this, TaskListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
   public Date toDate(String string)
   {
	   	String dateString = string;
	   	//String dateString = "01-12-1900";
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    Date convertedDate = new Date();
	    try {
	        convertedDate = dateFormat.parse(dateString);
	    } catch (ParseException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
		return convertedDate;
	   
   }

}
