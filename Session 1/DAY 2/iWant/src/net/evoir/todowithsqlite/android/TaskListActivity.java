package net.evoir.todowithsqlite.android;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import net.evoir.iwant.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

public class TaskListActivity extends Activity {

    private Context mContext;
    private Dao<Task, Integer> dao;
    private int item_id;
    private TaskAdapter taskAdapter;
    private List<Task> taskList;
    private Task task;
    private CheckBox taskStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_task_list);

        this.mContext = this;
        try {
            dao = Model.getHelper(this.mContext).getDao(Task.class);
            taskList = dao.queryForAll();
            final ListView taskListView = (ListView) this.findViewById(R.id.tasksList);
            taskAdapter = new TaskAdapter(this.mContext, taskList);
            taskListView.setAdapter(taskAdapter);
            taskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    task = taskList.get(position);
                    Intent intent = new Intent(TaskListActivity.this.mContext, TaskDetailActivity.class);
                    intent.putExtra("id", task.getId());
                    TaskListActivity.this.mContext.startActivity(intent);
                }

            });
            
            //register context menu for ListView
    		registerForContextMenu(taskListView);

    		

        } catch (SQLException e) {
            Log.e("TaskListActivity", e.getMessage());
        }

/*       final Button addButton = (Button) this.findViewById(R.id.addTaskButton);
       addButton.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent i = new Intent(TaskListActivity.this,TaskAddActivity.class);
 		   	startActivity(i);
			// TODO Auto-generated method stub
			
		}
	});*/


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.getMenuInflater().inflate(R.menu.task_list, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add) {
            startActivity(new Intent(mContext, TaskAddActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

   
    /**
     * MENU
     */

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
          super.onCreateContextMenu(menu, v, menuInfo);
          if (v.getId()==R.id.tasksList) {
        	  menu.setHeaderTitle("Options");  
              MenuInflater inflater = getMenuInflater();
              inflater.inflate(R.menu.long_click_menu, menu);
          }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
          AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
          task = taskList.get(info.position);
          final int position = task.getId();
          

          switch(item.getItemId()) {

             case R.id.menu_edit:

            	 //Toast.makeText(mContext, "Task has been edited", Toast.LENGTH_LONG).show();
             // add stuff here
            	 Intent i = new Intent(TaskListActivity.this,TaskEditActivity.class);
            	 i.putExtra("id", position);
            	startActivity(i);
               	Toast.makeText(mContext, "You're editing Task "+position, Toast.LENGTH_LONG).show();

                return true;
              case R.id.menu_delete:

      			try {
                  	DeleteBuilder<Task, Integer> deleteBuilder = dao.deleteBuilder();
                  	deleteBuilder.where().eq("id", position);
                  	deleteBuilder.delete();
                  	taskList.remove(info.position);

                  	taskAdapter.notifyDataSetChanged();

                  	Toast.makeText(mContext, "Task "+position+" has been deleted", Toast.LENGTH_LONG).show();
                  	
                  	//notify the adapter andout the change

      			} catch (SQLException e) {
      				// TODO Auto-generated catch block
      				e.printStackTrace();
      			}
                // edit stuff here
                    return true;
/*              case R.id.menu_add:
            // remove stuff here
                    return true;
*/              default:
                    return super.onContextItemSelected(item);
          }
    }
}
