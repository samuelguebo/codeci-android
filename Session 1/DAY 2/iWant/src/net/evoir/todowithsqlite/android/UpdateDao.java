package net.evoir.todowithsqlite.android;

import java.sql.SQLException;

import net.evoir.iwant.R;
import android.content.Context;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;

public class UpdateDao {
	private static Dao<Task, Integer> dao;
	private Context mContext;
	private Task task;
	public UpdateDao(Context context,Task task) {
		this.mContext = context;
		this.task = task;
		
		// I wish I could create a function for updating the Database quickly instead of call dao every time
		// I got stucked and failed..
        try {

            dao = Model.getHelper(mContext).getDao(Task.class);
        	UpdateBuilder<Task, Integer> updateBuilder = dao.updateBuilder();
        	
        	// set the criteria like you would a QueryBuilder
        	updateBuilder.where().eq("id", task.getId());
        	// update the value of the fields
        	updateBuilder.updateColumnValue("status", task.getStatus());
        	// update
        	updateBuilder.update();
        	/*Log.v("tag:Mytag","Task OnClick id is: "+task.getId());
        	Log.v("tag:Mytag","Task OnClick title is: "+vTitle.getText().toString());
        	Log.v("tag:Mytag","Task OnClick description is: "+vDescription.getText().toString());
        	Log.v("tag:Mytag","Task OnClick status is: "+vStatus.isChecked());
            Log.v("tag:Mytag","Task OnClick dueDate is: "+new Date());
*/
           
        } catch (SQLException e) {
            Log.e("TaskDetails", e.getMessage());
        }
	}

	
}
