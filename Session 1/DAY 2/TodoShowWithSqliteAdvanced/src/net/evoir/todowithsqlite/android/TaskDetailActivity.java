package net.evoir.todowithsqlite.android;

import java.sql.SQLException;

import net.evoir.androidtodo.advanced.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

public class TaskDetailActivity extends Activity {
	private Task task;
	private Context mContext = this;
	private static Dao<Task, Integer> dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_task_details);

        if (this.getIntent().getExtras() != null) {
            int id = this.getIntent().getExtras().getInt("id");
            Log.i("TaskDetailActivity", "ID DE LA TACHE EST : " + id);

            Dao<Task, Integer> dao;
            try {
                dao = Model.getHelper(this).getDao(Task.class);

                task = dao.queryForId(id);
                TextView vTitle,vDescription;
                vTitle = (TextView) this.findViewById(R.id.taskTitle_detail);
                vTitle.setText(task.getTitle());

                vDescription = (TextView) this.findViewById(R.id.taskDescription_detail);
                vDescription.setText(task.getDescription());

                //this.getActionBar().setTitle(task.getTitle()); 
            } catch (SQLException e) {
                Log.e("TaskDetails", e.getMessage());
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.getMenuInflater().inflate(R.menu.task_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_list) {
            startActivity(new Intent(TaskDetailActivity.this, TaskListActivity.class));
        }
        if (item.getItemId() == R.id.action_edit) {
        	Intent i = new Intent(TaskDetailActivity.this, TaskEditActivity.class);
        	i.putExtra("id", task.getId());
        	
        	startActivity(i);
        }
        if (item.getItemId() == R.id.action_delete) {
        	
        	
        	// Ask a validation
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
        }
        return super.onOptionsItemSelected(item);
    }
    
    	// handle answers
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
            case DialogInterface.BUTTON_POSITIVE:
                //Yes button clicked
            	//delete using dao
            	// we set an update builder
            	try {
					dao = Model.getHelper(mContext).getDao(Task.class);
	            	DeleteBuilder<Task, Integer> deleteBuilder = dao.deleteBuilder();
	            	deleteBuilder.where().eq("id", task.getId());
	            	deleteBuilder.delete();
	            	
	            	// prepare the new intent
	            	Intent i = new Intent(TaskDetailActivity.this, TaskListActivity.class);
	            	Toast.makeText(mContext, "Task has been deleted", Toast.LENGTH_LONG).show();
	            	startActivity(i);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            	
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                //No button clicked
            	Toast.makeText(mContext, "Deletion canceled", Toast.LENGTH_LONG).show();
                
                break;
            }
        }
    };

    
}
