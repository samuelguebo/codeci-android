package net.evoir.todowithsqlite.android;

import java.sql.SQLException;
import java.util.Date;

import net.evoir.androidtodo.advanced.R;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TaskEditActivity extends Activity {

	private static Dao<Task, Integer> dao;
	private static Task task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);
        // since id have been sent we just grab it
        if (this.getIntent().getExtras() != null) {
            int id = this.getIntent().getExtras().getInt("id");
            Log.i("TaskDetailActivity", "ID DE LA TACHE EST : " + id);

            try {
                dao = Model.getHelper(this).getDao(Task.class);

                task = dao.queryForId(id);
                EditText vTitle,vDescription;
                vTitle = (EditText) this.findViewById(R.id.taskTitle_edit);
                vDescription = (EditText) this.findViewById(R.id.taskDescription_edit);
                vTitle.setText(task.getTitle());
                vDescription.setText(task.getDescription());

                this.getActionBar().setTitle(task.getTitle()); 
            } catch (SQLException e) {
                Log.e("TaskDetails", e.getMessage());
            }
        }
        
        final EditText titleEditText = (EditText) findViewById(R.id.taskTitle_edit);
        final EditText descriptionEditText = (EditText) findViewById(R.id.taskDescription_edit);
        Button submitButton = (Button) findViewById(R.id.submitButton_edit);
        
        submitButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                if (titleEditText.getText().toString().isEmpty()) {
                    titleEditText.setError("Champ requis");
                } else if (descriptionEditText.getText().toString().isEmpty()) {
                    descriptionEditText.setError("Champ requis");
                } else {
                    try {
                    	// we set an update builder
                    	dao = Model.getHelper(TaskEditActivity.this).getDao(Task.class);
                    	UpdateBuilder<Task, Integer> updateBuilder = dao.updateBuilder();
                    	
                    	// set the criteria like you would a QueryBuilder
                    	updateBuilder.where().eq("id", task.getId());
                    	// update the value of the fields
                    	updateBuilder.updateColumnValue("title", titleEditText.getText().toString());
                    	updateBuilder.updateColumnValue("description", descriptionEditText.getText().toString());
                    	// update
                    	updateBuilder.update();
                    	Log.v("tag:Mytag","Task OnClick id is: "+task.getId());
                    	Log.v("tag:Mytag","Task OnClick title is: "+titleEditText.getText().toString());
                        Log.v("tag:Mytag","Task OnClick description is: "+descriptionEditText.getText().toString());

                        Toast.makeText(TaskEditActivity.this, "Task updated", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TaskEditActivity.this, TaskListActivity.class);
                        TaskEditActivity.this.startActivity(intent);
                    } catch (SQLException e) {
                        Log.e("Task EditActivity", e.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task_add, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_list) {
            startActivity(new Intent(TaskEditActivity.this, TaskListActivity.class));

        }
        return true;        
    } 
}
