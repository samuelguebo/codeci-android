package net.evoir.todowithsqlite.android;

import java.util.List;

import net.evoir.androidtodo.advanced.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TaskAdapter extends BaseAdapter {

    private List<Task> mTaskList;
    private Context mContext;
    private LayoutInflater mInflater;
    
    public TaskAdapter(Context context, List<Task> taskList) {
        this.mContext = context;
        this.mTaskList = taskList;
    }

    @Override
    public int getCount() {
        return mTaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return mTaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
//        return mTaskList.get(position).getId();
    }

    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {
        Task task = mTaskList.get(position);
        
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewCopy = mInflater.inflate(R.layout.activity_task_item, null);

        TextView textView = (TextView) viewCopy.findViewById(R.id.taskListItem);
        
        textView.setText(task.getTitle());
    
        return viewCopy;
        
    }

}
